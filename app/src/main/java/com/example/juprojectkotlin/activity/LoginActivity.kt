package com.example.juprojectkotlin.activity

import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.juprojectkotlin.R
import com.example.juprojectkotlin.model.Login
import com.example.juprojectkotlin.server.DestinationService
import com.example.juprojectkotlin.server.ServiceBuilder
import com.roger.catloadinglibrary.CatLoadingView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class LoginActivity : AppCompatActivity() {
    lateinit var email: String
    lateinit var password: String
    lateinit var etEmail: EditText
    lateinit var etPassword: EditText
    lateinit var btnLogin: Button
    lateinit var tvSignUp: TextView
    lateinit var tvCreatoneOne: TextView
    lateinit var tvTitle: TextView
    var loaderView: CatLoadingView? = null
    var mContext: Context? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        supportActionBar!!.hide()
        initialize()
        btnLogin.setOnClickListener(View.OnClickListener {
            if (validate()) {
                login()
            }
        })
    }

    private fun initialize() {
        mContext = this
        val regularFont = Typeface.createFromAsset(assets, "font/Roboto-Condensed.ttf")
        val titleFont = Typeface.createFromAsset(assets, "font/Roboto-BoldCondensed.ttf")

        loaderView = CatLoadingView()
        etEmail = findViewById(R.id.etUserName)
        etPassword = findViewById(R.id.etPassword)
        btnLogin = findViewById(R.id.btnLogin)
        tvSignUp = findViewById(R.id.tvSingUp)
        tvTitle = findViewById(R.id.tvLoginTitle)
        tvCreatoneOne = findViewById(R.id.tvNOtuser)

        etPassword.setTypeface(regularFont)
        etEmail.setTypeface(regularFont)
        tvTitle.setTypeface(titleFont)
        btnLogin.setTypeface(titleFont)
        tvSignUp.setTypeface(regularFont)
        tvCreatoneOne.setTypeface(regularFont)

        email = etEmail.text.toString().trim()
        password = etPassword.text.toString().trim()
    }

    private fun login() {
        loaderView!!.show(getSupportFragmentManager(), "");
        val destinationService: DestinationService =
            ServiceBuilder.builService(DestinationService::class.java)
        val login = destinationService.login(email, password)
        login.enqueue(object : Callback<Login> {


            override fun onResponse(call: Call<Login>, response: Response<Login>) {
                if (response.isSuccessful) {
                    if (response.code() == 200) {
                        val login: Login? = response.body()!!
                        Log.e("inside response", login.toString())
                        Log.e(" response code", response.code().toString())
                        loaderView!!.dismiss()
                        val itent = Intent(mContext, TeacherListActivity::class.java)
                        startActivity(itent)
                    }
                }
            }

            override fun onFailure(call: Call<Login>, t: Throwable) {
                Log.e("failur", t.message)
                loaderView!!.dismiss()
            }
        })
    }

    fun validate(): Boolean {
        var valid = false
        email = etEmail.getText().toString()
        password = etPassword.getText().toString()
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            valid = false
            etEmail.setError("Please enter valid email!")
        } else {
            valid = true
            etEmail.setError(null)
        }
        if (password.isEmpty()) {
            valid = false
            etPassword.setError("Please enter valid password!")
        } else {
            if (password.length > 5) {
                valid = true
                etPassword.setError(null)
            } else {
                valid = false
                etPassword.setError("Password is to short!")
            }
        }
        return valid
    }
}


