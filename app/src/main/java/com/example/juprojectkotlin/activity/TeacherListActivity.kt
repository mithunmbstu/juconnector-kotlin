package com.example.juprojectkotlin.activity

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.juprojectkotlin.R
import com.example.juprojectkotlin.model.Teacher
import com.example.juprojectkotlin.model.UserList
import com.example.juprojectkotlin.server.DestinationService
import com.example.juprojectkotlin.server.ServiceBuilder
import com.roger.catloadinglibrary.CatLoadingView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class TeacherListActivity : AppCompatActivity() {
    var loaderView: CatLoadingView? = null
    var mContext: Context? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.hide()
        initialize()
    }

    private fun initialize() {
        mContext = this;
        loaderView = CatLoadingView()

        getTeacherList()

    }

    private fun getTeacherList() {
        val destinationService: DestinationService =
            ServiceBuilder.builService(DestinationService::class.java)
        val teacherList = destinationService.getTeacherList()
        teacherList.enqueue(object : Callback<Teacher>{
            override fun onFailure(call: Call<Teacher>, t: Throwable) {

            }
           override fun onResponse(call: Call<Teacher>, response: Response<Teacher>) {
                var teacherArrayList = response.body()!!
                
            }

        })
    }
}
