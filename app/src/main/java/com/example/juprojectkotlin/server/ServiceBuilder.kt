package com.example.juprojectkotlin.server

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException

object ServiceBuilder {
    private const val URL="http://192.168.0.105:8000/api/"
    private val logger = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
    private val okHttpClient: OkHttpClient.Builder =OkHttpClient.Builder().addInterceptor(logger)
  
    private val builder:Retrofit.Builder=Retrofit.Builder().baseUrl(URL).
    addConverterFactory(GsonConverterFactory.create()).client(
        okHttpClient.build())
    private val retrofit:Retrofit= builder.build()

    fun<T>builService(serviceType :Class<T>):T{
        return retrofit.create(serviceType)
    }
}