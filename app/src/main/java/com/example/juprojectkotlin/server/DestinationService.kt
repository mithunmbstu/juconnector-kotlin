package com.example.juprojectkotlin.server

import com.example.juprojectkotlin.model.Login
import com.example.juprojectkotlin.model.Teacher
import com.example.juprojectkotlin.model.UserList
import retrofit2.Call
import retrofit2.http.*
import java.util.*
import kotlin.collections.ArrayList

interface DestinationService {
    @FormUrlEncoded
    @POST("login")
    fun login(@Field("email")email:String,@Field("password")password:String):Call<Login>

    @FormUrlEncoded
    @PUT("destination/{id}")
    fun updateDestination(
        @Path("id") id: Int, // here id is path.bcz id in curl bracket which is indicate path
        @Field("city") city: String,
        @Field("description") desc: String,
        @Field("country") country: String
    ): Call<Objects>

    @GET("teacherlist")
    fun getTeacherList():Call<Teacher>
    @GET("getuser")
    fun getUserlist():Call<UserList>

    /*if need particular item or resource then parametther is PATH*/
    /*if need filter with name or filter item then parametter is QUERY*/
}