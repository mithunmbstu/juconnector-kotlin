package com.example.juprojectkotlin.model

data class Success(
    val token: String,
    val users: Users
)