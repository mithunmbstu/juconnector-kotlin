package com.example.juprojectkotlin.model

data class Users(
    val batch: String,
    val created_at: String,
    val dept: String,
    val email: String,
    val email_verified_at: Any,
    val id: Int,
    val mobile: String,
    val name: String,
    val roll: String,
    val updated_at: String
)