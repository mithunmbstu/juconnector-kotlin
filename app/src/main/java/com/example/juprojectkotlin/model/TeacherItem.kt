package com.example.juprojectkotlin.model

data class TeacherItem(
    val created_at: Any,
    val dept: String,
    val id: Int,
    val teacher_name: String,
    val updated_at: Any
)