package com.example.juprojectkotlin.model

data class SuccessSignUp(
    val name: String,
    val token: String
)