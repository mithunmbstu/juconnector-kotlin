package com.example.juprojectkotlin.util

import android.app.Dialog
import android.content.Context
import android.view.Window
import android.widget.Button
import android.widget.TextView
import com.example.juprojectkotlin.R

class UserDialog {
    fun showUserAlert(context: Context?, message: String?, title: String?) {
        val dialog = Dialog(context)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.customuser_popoup_dialog)
        /* AlertDialog.Builder alert_builder = new AlertDialog.Builder(context);
        alert_builder.setView(view);
        final AlertDialog alert = alert_builder.create();*/
        val tvTitle = dialog.findViewById<TextView>(R.id.idDialogHeader)
        tvTitle.text = title
        val tvMessage = dialog.findViewById<TextView>(R.id.idAlertMessage)
        tvMessage.text = message
        val btnConfirm =
            dialog.findViewById<Button>(R.id.idConfirm)
        btnConfirm.setOnClickListener { dialog.dismiss() }
        dialog.show()
    }
}