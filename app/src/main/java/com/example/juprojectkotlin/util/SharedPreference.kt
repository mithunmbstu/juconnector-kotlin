package com.example.juprojectkotlin.util

import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import androidx.annotation.RequiresApi

class SharedPreference {
    var preferences: SharedPreferences? = null
    var editor: SharedPreferences.Editor? = null

    private val PREFS_NAME = "donate"
    val ROLE = "role"
    val UserId = "userid"
    val AUTHRIZATION = "auth"
    val Roll = "roll"
    val Name = "name"
    val Dept = "dept"
    val Batch = "batch"


    fun SharedPreference() {

        // TODO Auto-generated constructor stub
    }


    fun getBooleanValue(
        context: Context,
        key: String?
    ): Boolean {
        return context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
            .getBoolean(key, false)
    }


    @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
    fun setBooleanValue(
        context: Context,
        key: String?,
        status: Boolean?
    ) {
        val prefs = context.getSharedPreferences(
            PREFS_NAME, Context.MODE_PRIVATE
        )
        val editor = prefs.edit()
        editor.putBoolean(key, status!!)
        editor.apply()
    }


    fun getStringValue(context: Context, key: String?): String? {
        return context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
            .getString(key, "")
    }

    @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
    fun setStringValue(
        context: Context,
        key: String?,
        value: String?
    ) {
        val prefs = context.getSharedPreferences(
            PREFS_NAME, Context.MODE_PRIVATE
        )
        val editor = prefs.edit()
        editor.putString(key, value)
        editor.apply()
    }

    fun getIntValue(context: Context, key: String?): Int {
        return context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
            .getInt(key, 0)
    }

    @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
    fun setIntValue(
        context: Context, key: String?,
        value: Int
    ) {
        val prefs = context.getSharedPreferences(
            PREFS_NAME, Context.MODE_PRIVATE
        )
        val editor = prefs.edit()
        editor.putInt(key, value)
        editor.apply()
    }

    fun getLongValue(context: Context, key: String?): Long {
        return context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
            .getLong(key, 0)
    }

    @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
    fun setLongValue(
        context: Context,
        key: String?,
        value: Long
    ) {
        val prefs = context.getSharedPreferences(
            PREFS_NAME, Context.MODE_PRIVATE
        )
        val editor = prefs.edit()
        editor.putLong(key, value)
        editor.apply()
    }
}