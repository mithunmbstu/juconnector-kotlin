package com.example.juprojectkotlin.util

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.net.ConnectivityManager
import android.view.Window
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.example.juprojectkotlin.R

fun Activity.showToast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_LONG).show()
}

fun checkConn(ctx: Context): Boolean {
    return try {
        val conMgr =
            ctx.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val info = conMgr.activeNetworkInfo ?: return false
        if (!info.isConnected) return false
        if (!info.isAvailable) false else true
    } catch (e: Exception) {
        false
    }

}
